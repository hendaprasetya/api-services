/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.ffi.api.services.apiservice.param;

import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author henda
 */
public class RegionParam {

    @ApiModelProperty(notes = "Filter by Rsc Code", required =false)
    String rscCode = "";
    @ApiModelProperty(notes = "Filter by Region Code" , readOnly =false, required =false)
    String regionCode = "";

    public String getRscCode() {
        return rscCode;
    }

    public void setRscCode(String rscCode) {
        this.rscCode = rscCode;
    }

    public String getRegionCode() {
        return regionCode;
    }

    public void setRegionCode(String regionCode) {
        this.regionCode = regionCode;
    }
    
}
