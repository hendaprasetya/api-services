/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.ffi.api.services.apiservice.controller;

import com.api.ffi.api.services.apiservice.param.RegionParam;
import com.ffi.paging.Response;
import id.co.ffi.authRest.annotation.RestAuth;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author henda
 */
@RestController
@RequestMapping("/master")
@Api(value = "user", description = "Rest API for user operations", tags = "Master API")
public class FastEngineController {
 
    @Autowired
    RestTemplate restTemplate;
    
    @RestAuth
    @PostMapping("/role.action")
    @ApiOperation(value = "Contoh Response yang di Tampilkan", response = Object.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public String role(@RequestBody final String param){
        HttpEntity<String> request = new HttpEntity<>(param);
        ResponseEntity<String> quoteResponse = restTemplate.exchange("http://fast.ffi.co.id/role.action", HttpMethod.POST,
                request, new ParameterizedTypeReference<String>() {
                });
        return quoteResponse.getBody();
    }
    
    @PostMapping("/{req}")
    @ApiOperation(value = "Contoh Response yang di Tampilkan", response = Object.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public String get(@PathVariable String req, @RequestBody final String param){
        HttpEntity<String> request = new HttpEntity<>(param);
        ResponseEntity<String> quoteResponse = restTemplate.exchange("http://fast.ffi.co.id/"+req, HttpMethod.POST,
                request, new ParameterizedTypeReference<String>() {
                });
        return quoteResponse.getBody();
    }
    
    @PostMapping("/{req}/{req2}")
    @ApiOperation(value = "Contoh Response yang di Tampilkan", response = Object.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public Response get2(@PathVariable String req, @PathVariable String req2, @RequestBody final String param){
        HttpEntity<String> request = new HttpEntity<>(param);
        ResponseEntity<Response> quoteResponse = restTemplate.exchange("http://fast.ffi.co.id/"+req+"/"+req2, HttpMethod.POST,
                request, new ParameterizedTypeReference<Response>() {
                });
        return quoteResponse.getBody();
    }
    
    @GetMapping("/{req}")
    @ApiOperation(value = "Contoh Response yang di Tampilkan", response = Object.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public String getData(@PathVariable String req, final String param){
        HttpEntity<String> request = new HttpEntity<>(param);
        String prm = "";
        if(param !=null){
            prm = param;
        }
        ResponseEntity<String> quoteResponse = restTemplate.exchange("http://fast.ffi.co.id/"+req+"?param="+prm, HttpMethod.GET,
                request, new ParameterizedTypeReference<String>() {
                });
        return quoteResponse.getBody();
    }
    
    @GetMapping("/{req}/{req2}")
    @ApiOperation(value = "Contoh Response yang di Tampilkan", response = Object.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public Response getData2(@PathVariable String req, @PathVariable String req2, final String param){
        HttpEntity<String> request = new HttpEntity<>(param);
        String prm = "";
        if(param !=null){
            prm = param;
        }
        ResponseEntity<Response> quoteResponse = restTemplate.exchange("http://fast.ffi.co.id/"+req+"/"+req2+"?param="+prm, HttpMethod.GET,
                request, new ParameterizedTypeReference<Response>() {
                });
        return quoteResponse.getBody();
    }
}
