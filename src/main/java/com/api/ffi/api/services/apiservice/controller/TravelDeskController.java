/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.ffi.api.services.apiservice.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.io.IOException;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;

/**
 *
 * @author KFC SOLUTION
 */
@RestController
@RequestMapping("/traveldesk")
@Api(value = "user", description = "Rest API for user operations", tags = "FPC API")
public class TravelDeskController {

    @Autowired
    RestTemplate restTemplate;

    @PostMapping("/{req}")
    @ApiOperation(value = "Contoh Response yang di Tampilkan", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public String post(@PathVariable String req, @RequestBody final String param) {
        HttpEntity<String> request = new HttpEntity<>(param);
        ResponseEntity<String> quoteResponse = restTemplate.exchange("http://traveldesk.ffi.co.id/" + req, HttpMethod.POST,
                request, new ParameterizedTypeReference<String>() {
                });
        return quoteResponse.getBody();
    }

    @PostMapping("/data/{req}")
    @ApiOperation(value = "Contoh Response yang di Tampilkan", response = Object.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public String post(@PathVariable String req, MultipartHttpServletRequest param) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.MULTIPART_FORM_DATA);
        Iterator<String> itr = param.getFileNames();

        MultipartFile file = param.getFile(itr.next());
        MultiValueMap<String, Object> body  = new LinkedMultiValueMap<>();
        try {
            body.add("file", new ByteArrayResource(file.getBytes()));
        } catch (IOException ex) {
            Logger.getLogger(TravelDeskController.class.getName()).log(Level.SEVERE, null, ex);
        }
        body.add("param", param.getParameter("user"));
        HttpEntity<MultiValueMap<String, Object>> request = new HttpEntity<>(body, headers);
        ResponseEntity<String> quoteResponse = restTemplate.exchange("http://traveldesk.ffi.co.id/" + req, HttpMethod.POST,
                request, new ParameterizedTypeReference<String>() {
                });
        return quoteResponse.getBody();
    }
}
