/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.ffi.api.services.apiservice.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 * @RestController @RequestMapping("/oto")
 * @Api(value = "user", description = "Rest API for user operations", tags =
 * "Test API")
 *
 * @author KFC SOLUTION
 */
@RestController
@RequestMapping("/oto")
@Api(value = "user", description = "Rest API for user operations", tags = "API OTTO")
public class OttoController {

    @Autowired
    RestTemplate restTemplate;

    @PostMapping("/{action}")
    @ApiOperation(value = "Service OTTO PAY", response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public String payment(@PathVariable String action, @RequestBody final String param) {
        HttpEntity<String> prm = new HttpEntity<>(param);
        ResponseEntity<String> quoteResponse = restTemplate.exchange("http://oto.ffi.co.id/" + action, HttpMethod.POST,
                prm, new ParameterizedTypeReference<String>() {
                });
        return quoteResponse.getBody().toString();
    }

    @PostMapping("/tes")
    @ApiOperation(value = "Testing", response = String.class)
    @ApiResponses(value = {
        @ApiResponse(code = 200, message = "OK"),
        @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public String tes() {
        ResponseEntity<String> quoteResponse = restTemplate.exchange("http://oto.ffi.co.id/tes", HttpMethod.POST,
                null, new ParameterizedTypeReference<String>() {
                });

        String quotes = quoteResponse.getBody().toString();
        return quotes;
    }
}
