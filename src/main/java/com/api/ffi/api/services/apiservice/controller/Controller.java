/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.ffi.api.services.apiservice.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.List;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author henda
 */
@RestController
@RequestMapping("/api")
@Api(value = "user", description = "Rest API for user operations", tags = "Test API")
public class Controller {
    
    @Autowired
    RestTemplate restTemplate;
    
    @GetMapping("/master/city/{from}/to/{to}/find/{search}")
    @ApiOperation(value = "Contoh Response yang di Tampilkan", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public String get(@PathVariable("from") final Integer from, @PathVariable("to") final Integer to, @PathVariable("search") final String search){
        ResponseEntity<String> quoteResponse = restTemplate.exchange("http://localhost:8111/master/city/"+from+"/to/"+to+"/find/"+search, HttpMethod.GET,
                null, new ParameterizedTypeReference<String>() {
                });


        String quotes = quoteResponse.getBody().toString();
        String result = quotes;
        return result;
    }
    
    
    @GetMapping("/tes")
    @ApiOperation(value = "Testing", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public String tes() {
    	ResponseEntity<String> quoteResponse = restTemplate.exchange("http://master-db-services/master/tes", HttpMethod.GET,
                null, new ParameterizedTypeReference<String>() {
                });


        String quotes = quoteResponse.getBody().toString();
    	return quotes;
    }
    
    @GetMapping("/version")
    @ApiOperation(value = "Testing", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public String version() {
    	return "Api Services PT Fast Food Indonesia tbk. Ver.20190114";
    }
    
//    @GetMapping("/{username}")
//    public String getStock(@PathVariable("username") final String userName) {
//
//        ResponseEntity<List<String>> quoteResponse = restTemplate.exchange("http://localhost:8300/rest/db/" + userName, HttpMethod.GET,
//                null, new ParameterizedTypeReference<List<String>>() {
//                });
//
//
//        List<String> quotes = quoteResponse.getBody();
//        return quotes.get(0);
//    }
}
