/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.api.ffi.api.services.apiservice.controller;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

/**
 *
 * @author KFC SOLUTION
 */
@RestController
@RequestMapping("/asset")
@Api(value = "user", description = "Rest API for user operations", tags = "API ASSET")
public class AssetController {
    
    @Autowired
    RestTemplate restTemplate;
        
    @GetMapping("/{action}")
    @ApiOperation(value = "Action Asset Service", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public String action(@PathVariable String action, String param) {
        HttpEntity<String> request = new HttpEntity<String>(param);
    	ResponseEntity<String> quoteResponse = restTemplate.exchange("http://asset.ffi.co.id/"+action, HttpMethod.GET,
                request, new ParameterizedTypeReference<String>() {
                });
        return quoteResponse.getBody().toString();
    }
    
    @PostMapping("/trans/{action}")
    @ApiOperation(value = "Action Asset Service", response = String.class)
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 404, message = "The resource not found")
    }
    )
    public String actionPost(@PathVariable String action, @RequestBody final String param) {
        HttpEntity<String> request = new HttpEntity<String>(param);
    	ResponseEntity<String> quoteResponse = restTemplate.exchange("http://asset.ffi.co.id/"+action, HttpMethod.POST,
                request, new ParameterizedTypeReference<String>() {
                });
        return quoteResponse.getBody().toString();
    }
}
